﻿using System.Collections.Generic;

namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Holds case values for different game versions
    /// </summary>
    public static class CaseValues
    {
        #region Methods

        /// <summary>
        ///     Gets the correct values for version.
        /// </summary>
        /// <returns>correct list of values for chosen version of game</returns>
        public static IList<int> GetCorrectValuesForVersion()
        {
            switch (GameSettings.GameVersion)
            {
                case Versions.Syndicated:
                    return new List<int>(new[] {
                        0, 1, 5, 10, 25, 50, 75, 100, 200, 300, 400, 500, 750, 1000, 2500, 5000, 10000, 25000, 50000,
                        75000, 100000, 150000, 200000, 250000, 350000, 500000
                    });
                case Versions.Mega:
                    return new List<int>(new[] {
                        0, 100, 500, 1000, 2500, 5000, 7500, 10000, 20000, 30000, 40000, 50000, 75000, 100000, 225000,
                        400000, 500000, 750000, 1000000, 2000000, 3000000, 4000000, 5000000,
                        6000000, 8500000, 10000000
                    });
                default:
                    return new List<int>(new[] {
                        0, 1, 5, 10, 25, 50, 75, 100, 200, 300, 400, 500, 750, 1000, 5000, 10000, 25000, 50000, 75000,
                        100000, 200000, 300000, 400000, 500000, 750000, 1000000
                    });
            }
        }

        /// <summary>
        ///     Gets the highest case value for version.
        /// </summary>
        /// <returns>the highest case value for version</returns>
        public static int GetHighestCaseValueForVersion()
        {
            return GetCorrectValuesForVersion()[Constants.HighestCaseIndex];
        }

        #endregion
    }
}
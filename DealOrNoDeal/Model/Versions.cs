﻿namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Enum type for game versions
    /// </summary>
    public enum Versions
    {
        /// <summary>
        ///     Default game version
        /// </summary>
        Regular,

        /// <summary>
        ///     The syndicated game version
        /// </summary>
        Syndicated,

        /// <summary>
        ///     The mega game version
        /// </summary>
        Mega
    }
}
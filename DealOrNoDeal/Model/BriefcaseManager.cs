﻿using System;
using System.Collections.Generic;

namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Manages Briefcases
    /// </summary>
    public static class BriefcaseManager
    {
        #region Data members

        /// <summary>
        ///     The Briefcases
        /// </summary>
        public static IList<Briefcase> Briefcases;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the case initially selected.
        /// </summary>
        /// <value>
        ///     The case initially selected.
        /// </value>
        public static Briefcase CaseInitiallySelected { get; set; }

        #endregion

        #region Constructors

        static BriefcaseManager()
        {
            Briefcases = new List<Briefcase>();
            CaseInitiallySelected = null;
            populateCasesWithRandomValues();
        }

        #endregion

        #region Methods

        private static void populateCasesWithRandomValues()
        {
            var values = CaseValues.GetCorrectValuesForVersion();

            var random = new Random();
            var briefcaseId = 0;

            for (var i = Constants.HighestCaseIndex; i > -1; i--)
            {
                var randomValue = random.Next(0, i + 1);
                var briefcaseValue = values[randomValue];
                Briefcases.Add(new Briefcase(briefcaseId, briefcaseValue));
                values.RemoveAt(randomValue);
                briefcaseId++;
            }
        }

        private static int getCaseIndexFromId(int id)
        {
            if (id < 0 || id > Constants.HighestCaseIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(id),
                    $"must be between 0 and {Constants.HighestCaseIndex} inclusive");
            }

            var i = 0;
            foreach (var briefcase in Briefcases)
            {
                if (briefcase.Id == id)
                {
                    return i;
                }

                i++;
            }

            return -1;
        }

        /// <summary>
        ///     Removes the specified briefcase from play.
        ///     Precondition: id greater than/equals 0 AND id less than/equals {Constants.HighestCaseIndex}
        ///     Postcondition: briefcase with given ID removed from this.Briefcases
        /// </summary>
        /// <param name="id">The id of the briefcase to remove from play.</param>
        /// <returns>Dollar amount stored in the case, or -1 if case not found.</returns>
        public static int RemoveBriefcaseFromPlay(int id)
        {
            if (id < 0 || id > Constants.HighestCaseIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(id),
                    $"must be between 0 and {Constants.HighestCaseIndex} inclusive");
            }

            var briefcaseIndex = getCaseIndexFromId(id);

            if (briefcaseIndex != -1)
            {
                var dollarAmountStored = Briefcases[briefcaseIndex].DollarAmountStored;
                Briefcases.RemoveAt(briefcaseIndex);
                return dollarAmountStored;
            }

            return -1;
        }

        /// <summary>
        ///     Sums the values remaining.
        /// </summary>
        /// <returns>sum of remaining values</returns>
        public static int SumValuesRemaining()
        {
            var sum = 0;

            foreach (var briefcase in Briefcases)
            {
                sum += briefcase.DollarAmountStored;
            }

            sum += CaseInitiallySelected.DollarAmountStored;

            return sum;
        }

        #endregion
    }
}
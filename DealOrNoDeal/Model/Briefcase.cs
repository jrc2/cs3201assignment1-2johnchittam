﻿using System;

namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Handles Briefcases in game
    /// </summary>
    public class Briefcase
    {
        #region Properties

        /// <summary>
        ///     Gets the case identifier.
        /// </summary>
        /// <value>
        ///     The case identifier.
        /// </value>
        public int Id { get; }

        /// <summary>
        ///     Gets the dollar amount stored.
        /// </summary>
        /// <value>
        ///     The dollar amount stored.
        /// </value>
        public int DollarAmountStored { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Briefcase" /> class.
        ///     Precondition: id greater than/equals 0 AND id less than/equals {Constants.HighestCaseIndex};
        ///     dollarAmountStored greater than/equals 0 AND
        ///     dollarAmountStored less than/equals {Constants.HighestCaseValue}
        ///     Postcondition: this.Id==id; this.DollarAmountStored==dollarAmountStored;
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dollarAmountStored">The dollar amount stored.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// </exception>
        public Briefcase(int id, int dollarAmountStored)
        {
            if (id < 0 || id > Constants.HighestCaseIndex)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (dollarAmountStored < 0 || dollarAmountStored > CaseValues.GetHighestCaseValueForVersion())
            {
                throw new ArgumentOutOfRangeException();
            }

            this.Id = id;
            this.DollarAmountStored = dollarAmountStored;
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Controls the Banker
    /// </summary>
    public class Banker
    {
        #region Properties

        /// <summary>
        ///     Gets the offers.
        /// </summary>
        /// <value>
        ///     The offers.
        /// </value>
        public IList<int> Offers { get; }

        /// <summary>
        ///     Gets the average offer.
        /// </summary>
        /// <value>
        ///     The average offer.
        /// </value>
        public int AverageOffer => this.roundToNearest100((int) this.Offers.Average());

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Banker" /> class.
        ///     Postcondition: Offers==new empty List of int
        /// </summary>
        public Banker()
        {
            this.Offers = new List<int>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Calculates the offer.
        ///     Precondition: numCasesToOpenInNextRound>0
        ///     Postcondition: Offers contains offer
        /// </summary>
        /// <param name="numCasesToOpenInNextRound">The number cases to open in next round.</param>
        /// <returns>the offer, based on the cases' values and number of cases to be opened</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     numCasesToOpenInNextRound - must be opening at least one case
        /// </exception>
        public int ProcessNewOffer(int numCasesToOpenInNextRound)
        {
            if (numCasesToOpenInNextRound <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(numCasesToOpenInNextRound),
                    "must be opening at least one case");
            }

            var offer = calculateOffer(numCasesToOpenInNextRound);
            offer = this.roundToNearest100(offer);
            this.Offers.Add(offer);
            return offer;
        }

        private static int calculateOffer(int numCasesToOpenInNextRound)
        {
            var offer = BriefcaseManager.SumValuesRemaining() / numCasesToOpenInNextRound /
                        (BriefcaseManager.Briefcases.Count + 1);
            return offer;
        }

        private int roundToNearest100(int numberToRound)
        {
            return (numberToRound + 50) / 100 * 100;
        }

        #endregion
    }
}
﻿namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Contains constants used throughout the program
    /// </summary>
    public sealed class Constants
    {
        #region Data members

        /// <summary>
        ///     The ID if case not selected
        /// </summary>
        public const int CaseNotSelectedId = -1;

        /// <summary>
        ///     The highest case index
        /// </summary>
        public const int HighestCaseIndex = 25;

        #endregion
    }
}
﻿namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Enum type for number of rounds
    /// </summary>
    public enum NumberOfRounds
    {
        /// <summary>
        ///     10 rounds
        /// </summary>
        Default = 10,

        /// <summary>
        ///     7 rounds
        /// </summary>
        Seven = 7,

        /// <summary>
        ///     13 rounds
        /// </summary>
        Thirteen = 13
    }
}
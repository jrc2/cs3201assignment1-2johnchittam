﻿namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Holds the game settings
    /// </summary>
    public static class GameSettings
    {
        #region Properties

        /// <summary>
        ///     Gets the game version.
        /// </summary>
        /// <value>
        ///     The game version.
        /// </value>
        public static Versions GameVersion { get; }

        /// <summary>
        ///     Gets the number of rounds.
        /// </summary>
        /// <value>
        ///     The number of rounds.
        /// </value>
        public static NumberOfRounds NumberOfRounds { get; }

        #endregion

        #region Constructors

        static GameSettings()
        {
            GameVersion = Versions.Regular;
//            GameVersion = Versions.Syndicated;
//            GameVersion = Versions.Mega;

            NumberOfRounds = NumberOfRounds.Default;
//            NumberOfRounds = NumberOfRounds.Seven;
//            NumberOfRounds = NumberOfRounds.Thirteen;
        }

        #endregion
    }
}
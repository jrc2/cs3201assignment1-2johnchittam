﻿namespace DealOrNoDeal.Model
{
    /// <summary>
    ///     Controls the game rounds
    /// </summary>
    public class RoundManager
    {
        #region Data members

        private int numCasesInFirstRound;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the initial cases in round.
        /// </summary>
        /// <value>
        ///     The initial cases in round.
        /// </value>
        public int InitialCasesInRound { get; set; }

        /// <summary>
        ///     Gets or sets the cases to be opened.
        /// </summary>
        /// <value>
        ///     The cases to be opened.
        /// </value>
        public int CasesToBeOpenedThisRound { get; set; }

        /// <summary>
        ///     Gets or sets the current round number.
        /// </summary>
        /// <value>
        ///     The current round number.
        /// </value>
        public int CurrentRoundNumber { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoundManager" /> class.
        /// </summary>
        public RoundManager()
        {
            this.findNumCasesInFirstRound();
            this.InitialCasesInRound = this.numCasesInFirstRound;
            this.CurrentRoundNumber = 1;
            this.CasesToBeOpenedThisRound = this.numCasesInFirstRound;
        }

        #endregion

        #region Methods

        private void findNumCasesInFirstRound()
        {
            switch (GameSettings.NumberOfRounds)
            {
                case NumberOfRounds.Seven:
                    this.numCasesInFirstRound = 8;
                    break;
                case NumberOfRounds.Thirteen:
                    this.numCasesInFirstRound = 7;
                    break;
                default:
                    this.numCasesInFirstRound = 6;
                    break;
            }
        }

        /// <summary>
        ///     Moves to next round by incrementing Round property and setting
        ///     initial number of cases for that round
        ///     Postcondition: Round == Round@prev + 1 AND CasesRemainingInRound == (number of cases to open in the next round)
        /// </summary>
        public void MoveToNextRound()
        {
            this.CurrentRoundNumber++;

            switch (GameSettings.NumberOfRounds)
            {
                case NumberOfRounds.Seven:
                    if (this.InitialCasesInRound >= 6)
                    {
                        this.InitialCasesInRound -= 2;
                    }
                    else if (this.InitialCasesInRound > 1)
                    {
                        this.InitialCasesInRound--;
                    }

                    break;
                case NumberOfRounds.Thirteen:
                    if (this.InitialCasesInRound > 1)
                    {
                        this.InitialCasesInRound -= 2;
                    }

                    break;
                default:
                    if (this.InitialCasesInRound != 1)
                    {
                        this.InitialCasesInRound--;
                    }

                    break;
            }

            this.CasesToBeOpenedThisRound = this.InitialCasesInRound;
        }

        /// <summary>
        ///     Calculates the number of cases to open next round.
        /// </summary>
        /// <returns>the number of cases to open next round</returns>
        public int NumCasesToOpenNextRound()
        {
            if (this.InitialCasesInRound > 1)
            {
                return this.InitialCasesInRound - 1;
            }

            return this.InitialCasesInRound;
        }

        #endregion
    }
}
﻿using System.Collections.Generic;

namespace DealOrNoDeal.Model
{
    /// <summary>Handles the management of the actual game play.</summary>
    public class GameManager
    {
        #region Data members

        private readonly Banker banker = new Banker();
        private readonly RoundManager roundManager = new RoundManager();

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the current round number from roundManager.
        /// </summary>
        /// <value>
        ///     The current round number.
        /// </value>
        public int CurrentRoundNumber => this.roundManager.CurrentRoundNumber;

        /// <summary>
        ///     Gets the initial cases in round from roundManager.
        /// </summary>
        /// <value>
        ///     The initial cases in round.
        /// </value>
        public int InitialCasesInRound => this.roundManager.InitialCasesInRound;

        /// <summary>
        ///     Gets or sets the cases to be opened in roundManager.
        /// </summary>
        /// <value>
        ///     The cases to be opened.
        /// </value>
        public int CasesToBeOpenedThisRound
        {
            get => this.roundManager.CasesToBeOpenedThisRound;
            set => this.roundManager.CasesToBeOpenedThisRound = value;
        }

        /// <summary>
        ///     Gets or sets the case initially selected in roundManager.
        /// </summary>
        /// <value>
        ///     The case initially selected.
        /// </value>
        public Briefcase CaseInitiallySelected
        {
            get => BriefcaseManager.CaseInitiallySelected;
            set => BriefcaseManager.CaseInitiallySelected = value;
        }

        /// <summary>
        ///     Gets a new offer from banker.
        /// </summary>
        /// <value>
        ///     The new offer from banker.
        /// </value>
        public int GetNewOfferFromBanker =>
            this.banker.ProcessNewOffer(this.roundManager.NumCasesToOpenNextRound());

        /// <summary>
        ///     Gets the last offer from banker.
        /// </summary>
        /// <value>
        ///     The last offer from banker.
        /// </value>
        public int LastOfferFromBanker => this.banker.Offers[this.banker.Offers.Count - 1];

        /// <summary>
        ///     Gets the average offer from banker.
        /// </summary>
        /// <value>
        ///     The average offer from banker.
        /// </value>
        public int AverageOfferFromBanker => this.banker.AverageOffer;

        /// <summary>
        ///     Gets the offers from banker.
        /// </summary>
        /// <value>
        ///     The offers from banker.
        /// </value>
        public IList<int> OffersFromBanker => this.banker.Offers;

        #endregion

        #region Methods

        /// <summary>
        ///     Tells roundManager to move to the next round
        /// </summary>
        public void MoveToNextRound()
        {
            this.roundManager.MoveToNextRound();
        }

        #endregion
    }
}
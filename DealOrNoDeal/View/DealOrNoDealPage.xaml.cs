﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using DealOrNoDeal.Model;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DealOrNoDeal.View
{
    /// <summary>
    ///     An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DealOrNoDealPage
    {
        #region Data members

        /// <summary>
        ///     The application height
        /// </summary>
        public const int ApplicationHeight = 600;

        /// <summary>
        ///     The application width
        /// </summary>
        public const int ApplicationWidth = 600;

        /// <summary>
        ///     The game manager
        /// </summary>
        public GameManager GameManager;

        private IList<Button> briefcaseButtons;
        private IList<Border> dollarAmountLabels;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DealOrNoDealPage" /> class.
        /// </summary>
        public DealOrNoDealPage()
        {
            this.InitializeComponent();
            this.initializeUiDataAndControls();
            this.GameManager = new GameManager();
        }

        #endregion

        #region Methods

        private void initializeUiDataAndControls()
        {
            this.setPageSize();

            this.briefcaseButtons = new List<Button>();
            this.dollarAmountLabels = new List<Border>();
            this.buildBriefcaseButtonCollection();
            this.buildDollarAmountLabelCollection();
            this.setupWelcomeText();
        }

        private void setPageSize()
        {
            ApplicationView.PreferredLaunchViewSize = new Size {Width = ApplicationWidth, Height = ApplicationHeight};
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(ApplicationWidth, ApplicationHeight));
        }

        private void buildDollarAmountLabelCollection()
        {
            this.dollarAmountLabels.Clear();

            this.dollarAmountLabels.Add(this.label0Border);
            this.dollarAmountLabels.Add(this.label1Border);
            this.dollarAmountLabels.Add(this.label2Border);
            this.dollarAmountLabels.Add(this.label3Border);
            this.dollarAmountLabels.Add(this.label4Border);
            this.dollarAmountLabels.Add(this.label5Border);
            this.dollarAmountLabels.Add(this.label6Border);
            this.dollarAmountLabels.Add(this.label7Border);
            this.dollarAmountLabels.Add(this.label8Border);
            this.dollarAmountLabels.Add(this.label9Border);
            this.dollarAmountLabels.Add(this.label10Border);
            this.dollarAmountLabels.Add(this.label11Border);
            this.dollarAmountLabels.Add(this.label12Border);
            this.dollarAmountLabels.Add(this.label13Border);
            this.dollarAmountLabels.Add(this.label14Border);
            this.dollarAmountLabels.Add(this.label15Border);
            this.dollarAmountLabels.Add(this.label16Border);
            this.dollarAmountLabels.Add(this.label17Border);
            this.dollarAmountLabels.Add(this.label18Border);
            this.dollarAmountLabels.Add(this.label19Border);
            this.dollarAmountLabels.Add(this.label20Border);
            this.dollarAmountLabels.Add(this.label21Border);
            this.dollarAmountLabels.Add(this.label22Border);
            this.dollarAmountLabels.Add(this.label23Border);
            this.dollarAmountLabels.Add(this.label24Border);
            this.dollarAmountLabels.Add(this.label25Border);

            this.setLabelsToCorrectDollarAmounts();
        }

        private void setLabelsToCorrectDollarAmounts()
        {
            for (var i = 0; i <= Constants.HighestCaseIndex; i++)
            {
                if (this.dollarAmountLabels[i].Child is TextBlock dollarTextBlock)
                {
                    var numberFormat = new CultureInfo("en-US", false).NumberFormat;
                    numberFormat.CurrencyDecimalDigits = 0;
                    dollarTextBlock.Text = CaseValues.GetCorrectValuesForVersion()[i].ToString("C", numberFormat);
                }
            }
        }

        private void buildBriefcaseButtonCollection()
        {
            this.briefcaseButtons.Clear();

            this.briefcaseButtons.Add(this.case0);
            this.briefcaseButtons.Add(this.case1);
            this.briefcaseButtons.Add(this.case2);
            this.briefcaseButtons.Add(this.case3);
            this.briefcaseButtons.Add(this.case4);
            this.briefcaseButtons.Add(this.case5);
            this.briefcaseButtons.Add(this.case6);
            this.briefcaseButtons.Add(this.case7);
            this.briefcaseButtons.Add(this.case8);
            this.briefcaseButtons.Add(this.case9);
            this.briefcaseButtons.Add(this.case10);
            this.briefcaseButtons.Add(this.case11);
            this.briefcaseButtons.Add(this.case12);
            this.briefcaseButtons.Add(this.case13);
            this.briefcaseButtons.Add(this.case14);
            this.briefcaseButtons.Add(this.case15);
            this.briefcaseButtons.Add(this.case16);
            this.briefcaseButtons.Add(this.case17);
            this.briefcaseButtons.Add(this.case18);
            this.briefcaseButtons.Add(this.case19);
            this.briefcaseButtons.Add(this.case20);
            this.briefcaseButtons.Add(this.case21);
            this.briefcaseButtons.Add(this.case22);
            this.briefcaseButtons.Add(this.case23);
            this.briefcaseButtons.Add(this.case24);
            this.briefcaseButtons.Add(this.case25);

            this.storeBriefCaseIndexInControlsTagProperty();
        }

        private void setupWelcomeText()
        {
            if (GameSettings.GameVersion == Versions.Regular)
            {
                this.roundLabel.Text = "Welcome to Deal or No Deal!";
            }
            else
            {
                this.roundLabel.Text = $"Welcome to the {GameSettings.GameVersion} version!";
            }
        }

        private void storeBriefCaseIndexInControlsTagProperty()
        {
            for (var i = 0; i < this.briefcaseButtons.Count; i++)
            {
                this.briefcaseButtons[i].Tag = i;
            }
        }

        private void briefcase_Click(object sender, RoutedEventArgs e)
        {
            var senderButton = (Button) sender;
            var briefcaseId = this.getBriefcaseID(senderButton);
            var dollarAmount = BriefcaseManager.RemoveBriefcaseFromPlay(briefcaseId);

            if (this.isFinalRoundCase(senderButton, dollarAmount))
            {
                return;
            }

            senderButton.IsEnabled = false;
            senderButton.Visibility = Visibility.Collapsed;

            this.handleInitialCaseSelection(briefcaseId, dollarAmount);

            this.updateCurrentRoundInformation();
        }

        private void handleInitialCaseSelection(int briefcaseId, int dollarAmount)
        {
            if (this.GameManager.CaseInitiallySelected == null)
            {
                this.GameManager.CaseInitiallySelected = new Briefcase(briefcaseId, dollarAmount);
                this.summaryOutput.Text = $"Your case: {briefcaseId + 1}";
            }
            else
            {
                this.findAndGrayOutGameDollarLabel(dollarAmount);
                this.GameManager.CasesToBeOpenedThisRound--;
            }
        }

        private bool isFinalRoundCase(Button senderButton, int dollarAmount)
        {
            if (senderButton.Name == "finalBriefcase")
            {
                this.handleEndOfGame(dollarAmount);
                return true;
            }

            if (senderButton.Name == "initialBriefcase")
            {
                this.handleEndOfGame(this.GameManager.CaseInitiallySelected.DollarAmountStored);
                return true;
            }

            return false;
        }

        private void findAndGrayOutGameDollarLabel(int amount)
        {
            foreach (var currDollarAmountLabel in this.dollarAmountLabels)
            {
                if (grayOutLabelIfMatchesDollarAmount(amount, currDollarAmountLabel))
                {
                    break;
                }
            }
        }

        private static bool grayOutLabelIfMatchesDollarAmount(int amount, Border currDollarAmountLabel)
        {
            var matched = false;

            if (currDollarAmountLabel.Child is TextBlock dollarTextBlock)
            {
                var labelAmount = int.Parse(dollarTextBlock.Text, NumberStyles.Currency);
                if (labelAmount == amount)
                {
                    currDollarAmountLabel.Background = new SolidColorBrush(Colors.Gray);
                    matched = true;
                }
            }

            return matched;
        }

        private int getBriefcaseID(Button selectedBriefCase)
        {
            return (int) selectedBriefCase.Tag;
        }

        private void updateCurrentRoundInformation()
        {
            if (this.GameManager.CasesToBeOpenedThisRound == 0)
            {
                this.processEndOfRound();
            }

            if (this.GameManager.CurrentRoundNumber != (int) GameSettings.NumberOfRounds)
            {
                this.roundLabel.Text =
                    $"Round {this.GameManager.CurrentRoundNumber}: {this.GameManager.InitialCasesInRound} " +
                    $"case{this.returnSIfPluralNeeded(this.GameManager.InitialCasesInRound)} to open";
                this.casesToOpenLabel.Text =
                    $"{this.GameManager.CasesToBeOpenedThisRound} more " +
                    $"case{this.returnSIfPluralNeeded(this.GameManager.CasesToBeOpenedThisRound)} to open";
            }
            else
            {
                this.processFinalRoundDisplay();
            }
        }

        private string returnSIfPluralNeeded(int count)
        {
            if (count == 1)
            {
                return "";
            }

            return "s";
        }

        private void processFinalRoundDisplay()
        {
            this.roundLabel.Text = "This is the final round.";
            this.casesToOpenLabel.Text = "Select a case above.";

            var finalBriefcaseButton = this.separateFinalBriefcaseButton();
            var initialBriefcaseButton = this.separateInitialBriefcaseButton();

            this.placeFinalRoundButtons(finalBriefcaseButton, initialBriefcaseButton);
        }

        private void placeFinalRoundButtons(Button finalBriefcaseButton, Button initialBriefcaseButton)
        {
            var finalBriefcaseId = Constants.CaseNotSelectedId;
            if (finalBriefcaseButton.Content != null)
            {
                finalBriefcaseId = int.Parse(finalBriefcaseButton.Content.ToString());
            }

            if (finalBriefcaseId < BriefcaseManager.CaseInitiallySelected.Id)
            {
                this.centerPanel.Children.Add(finalBriefcaseButton);
            }

            this.centerPanel.Children.Add(initialBriefcaseButton);

            if (finalBriefcaseId > BriefcaseManager.CaseInitiallySelected.Id)
            {
                this.centerPanel.Children.Add(finalBriefcaseButton);
            }

            initialBriefcaseButton.Visibility = Visibility.Visible;
        }

        private Button separateInitialBriefcaseButton()
        {
            var initialBriefcaseButton = this.briefcaseButtons[0];
            foreach (var briefcaseButton in this.briefcaseButtons)
            {
                if (briefcaseButton.Content != null && briefcaseButton.Content.ToString() ==
                    (this.GameManager.CaseInitiallySelected.Id + 1).ToString())
                {
                    initialBriefcaseButton = briefcaseButton;
                    initialBriefcaseButton.Name = "initialBriefcase";
                    break;
                }
            }

            var initialBriefcaseParent = (StackPanel) initialBriefcaseButton.Parent;
            initialBriefcaseParent.Children.Remove(initialBriefcaseButton);
            return initialBriefcaseButton;
        }

        private Button separateFinalBriefcaseButton()
        {
            var finalBriefcaseButton = this.briefcaseButtons[0];
            foreach (var briefcaseButton in this.briefcaseButtons)
            {
                if (briefcaseButton.Visibility == Visibility.Visible)
                {
                    finalBriefcaseButton = briefcaseButton;
                    finalBriefcaseButton.Name = "finalBriefcase";
                    break;
                }
            }

            var finalBriefcaseParent = (StackPanel) finalBriefcaseButton.Parent;
            finalBriefcaseParent.Children.Remove(finalBriefcaseButton);
            return finalBriefcaseButton;
        }

        private void processEndOfRound()
        {
            foreach (var briefcaseButton in this.briefcaseButtons)
            {
                briefcaseButton.IsEnabled = false;
            }

            var currentOffer = this.GameManager.GetNewOfferFromBanker.ToString("C", CultureInfo.CurrentCulture);
            this.summaryOutput.Text =
                $"{this.generateOfferStats()}\n" +
                $"Current offer: {currentOffer}\n" +
                "Deal or No Deal?";
            this.displayDealAndNoDealButtons();
        }

        private void dealButton_Click(object sender, RoutedEventArgs e)
        {
            this.collapseDealAndNoDealButtons();
            this.summaryOutput.Text =
                $"Your case contained: {this.GameManager.CaseInitiallySelected.DollarAmountStored.ToString("C", CultureInfo.CurrentCulture)}\n" +
                $"Accepted offer: {this.GameManager.LastOfferFromBanker.ToString("C", CultureInfo.CurrentCulture)}\n" +
                "GAME OVER";
            this.enableApplicationRestartPopup();
        }

        private void noDealButton_Click(object sender, RoutedEventArgs e)
        {
            this.GameManager.MoveToNextRound();

            foreach (var briefcaseButton in this.briefcaseButtons)
            {
                briefcaseButton.IsEnabled = true;
            }

            this.collapseDealAndNoDealButtons();
            this.updateCurrentRoundInformation();
            this.summaryOutput.Text =
                $"{this.generateOfferStats()}\n" +
                $"Last offer: {this.GameManager.LastOfferFromBanker.ToString("C", CultureInfo.CurrentCulture)}";
        }

        private string generateOfferStats()
        {
            var minOffer = this.GameManager.OffersFromBanker.Min().ToString("C", CultureInfo.CurrentCulture);
            var maxOffer = this.GameManager.OffersFromBanker.Max().ToString("C", CultureInfo.CurrentCulture);
            var averageOffer = this.GameManager.AverageOfferFromBanker.ToString("C", CultureInfo.CurrentCulture);
            var output =
                $"Offers: Max: {maxOffer}; Min: {minOffer}\n" +
                $"        Average: {averageOffer}";
            return output;
        }

        private void collapseDealAndNoDealButtons()
        {
            this.dealButton.Visibility = Visibility.Collapsed;
            this.noDealButton.Visibility = Visibility.Collapsed;
        }

        private void displayDealAndNoDealButtons()
        {
            this.dealButton.Visibility = Visibility.Visible;
            this.noDealButton.Visibility = Visibility.Visible;
        }

        private void handleEndOfGame(int winningsAmount)
        {
            this.handleEndOfGameSummaryText(winningsAmount);
            this.collapseDealAndNoDealButtons();
            this.enableApplicationRestartPopup();
        }

        private async void enableApplicationRestartPopup()
        {
            var playAgainDialog = new ContentDialog {
                Title = "Game Over",
                Content = "Do you want to play again?",
                PrimaryButtonText = "Yes",
                CloseButtonText = "No"
            };

            var restartResult = await playAgainDialog.ShowAsync();

            switch (restartResult)
            {
                case ContentDialogResult.Primary:
                    this.restartApplication();
                    break;
            }
        }

        private void handleEndOfGameSummaryText(int winningsAmount)
        {
            this.summaryOutput.Text =
                $"Congrats you win {winningsAmount.ToString("C", CultureInfo.CurrentCulture)}\n\n" +
                "GAME OVER";
        }

        private async void restartApplication()
        {
            await CoreApplication.RequestRestartAsync("Application Restart Programmatically ");
        }

        #endregion
    }
}